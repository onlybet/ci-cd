# Deploiement de l'infrastructure


### Prérequis:
- Terraform
- Ansible
- Git

### Création de deux instances AWS avec Terraform

> :bulb:  Terraform est un outil qui va nous permettre de créer deux instances sur AWS (Amazon web service) 
> - Une instance pour accueillir le **front** de l'application
> - Une instance pour accueillir le **back** de l'application


Après avoir cloné le projet dans votre repertoire de travail, deplacez-vous dans le repertoire **/terraform-ansible-template** pour commencer le deploiement.

>:warning: Pour pouvoir vous connecter sur la plateform AWS, vous devez integrez vos identifiants AWS dans votre repertoire local **.aws** 
> Voici les troix variables qui doivent etre présente dans votre répertoire:
> - aws_access_key_id
> - aws_secret_access_key
> - aws_session_token

Initialisez le contexte terraform avec la commande suivante:
```bash
terraform init
```

Verifiez que la configuation est bonne avec les commandes suivantes:
```bash
terraform validate
terraform plan
```

Une fois que tout est bon, vous pouvez lancer le deploiement des deux instances avec la commande:
```bash
terraform apply
```
Les instance sont maintenant présentes sur le cloud AWS

### Configuration  des instances sur Ansible

Maitenant que vos instances sont présentes sur AWS, il faut les configurer.

Deplacez vous dans le répertoire **terraform-ansible-template/ansible***

Lancez la commande suivante:

```bash
ansible-playbook playbook.yml
```


> Cette commande va lancer le playbook ansible qui va permettre de configurer les deux instances comme ceci:
> - Installation et configuration de Docker sur les deux instances
> - Clone de du front dans un container docker
> - Clone du back dans un container docker



