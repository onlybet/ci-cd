
# Allo SSH ingress
resource "aws_security_group" "allow_ssh_group" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
}

resource "aws_security_group_rule" "allow_ssh" {
  security_group_id = aws_security_group.allow_ssh_group.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

# Allo 19006 ingress: prod default port expo react native app
resource "aws_security_group" "allow_19006_group" {
  name        = "allow_19006"
  description = "Allow 19006 prod default port inbound traffic"
}

resource "aws_security_group_rule" "allow_19006" {
  security_group_id = aws_security_group.allow_19006_group.id
  type              = "ingress"
  from_port         = 19006
  to_port           = 19006
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}


# Allo 80 & 8080 ingress
resource "aws_security_group" "allow_web_group" {
  name        = "allow_80_8080"
  description = "Allow 80 & 8080 inbound traffic"
}

resource "aws_security_group_rule" "allow_ssh" {
  security_group_id = aws_security_group.allow_web_group.id
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "allow_ssh" {
  security_group_id = aws_security_group.allow_web_group.id
  type              = "ingress"
  from_port         = 8080
  to_port           = 8080
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}