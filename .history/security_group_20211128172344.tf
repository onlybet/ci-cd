
# Allo SSH ingress
resource "aws_security_group" "allow_ssh_group" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
}

resource "aws_security_group_rule" "allow_ssh" {
  security_group_id = aws_security_group.allow_ssh_group.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

# Allo SSH ingress
resource "aws_security_group" "allow_19006_group" {
  name        = "allow_19006"
  description = "Allow 19006 prod default port inbound traffic"
}

resource "aws_security_group_rule" "allow_19006" {
  security_group_id = aws_security_group.allow_19006_group.id
  type              = "ingress"
  from_port         = 19006
  to_port           = 19006
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}
#--------------------------------------------------------------------------------------------


resource "openstack_networking_secgroup_rule_v2" "allow_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ssh_icmp_IT2.id
}


#créer un group accepte http et allow port 80

resource "openstack_networking_secgroup_rule_v2" "allow_80" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.allow_web_IT2.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_8080" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  security_group_id = openstack_networking_secgroup_v2.allow_web_IT2.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_3306" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  security_group_id = openstack_networking_secgroup_v2.allow_web_IT2.id
  remote_group_id = openstack_networking_secgroup_v2.allow_web_IT2.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ssh_icmp_IT2.id
}