all:
  children:
    front:
      hosts:
        ${front.tags["Name"]}:
          ansible_host: ${front.public_ip}
