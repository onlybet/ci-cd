# resource "local_file" "ansible_inventory" {
#   content = templatefile("ansible_template.tpl",
#     {
#       front = aws_instance.front_server
#     }
#   )
#   filename = "ansible/inventory/hosts.yml"
# }

output "instance_id" {
  description = "ID of the FRONT EC2 instance"
  value       = aws_instance.front_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the FRONT EC2 instance"
  value       = aws_instance.front_server.public_ip
}