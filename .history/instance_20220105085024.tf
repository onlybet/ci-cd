data "aws_ami" "latest-ubuntu" {
  most_recent = true
  owners      = ["327094444948"] # AWS

  filter {
    name   = "name"
    values = ["Cloud9Ubuntu-202*"]
  }
}


// Instance FRONT
resource "aws_instance" "front_server" {
  ami                    = data.aws_ami.latest-ubuntu.id
  instance_type          = "t2.nano"
  key_name               = aws_key_pair.key.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh_group.id,aws_security_group.allow_19006_group.id,aws_security_group.allow_all_egress.id,aws_security_group.allow_85_group.id]
  root_block_device  {
       volume_size = "50"
  }
  tags = {
    Name = "front"
  }
}

resource "aws_instance" "node_server" {
  count                  = 2
  ami                    = data.aws_ami.latest-ubuntu.id
  instance_type          = "t2.nano"
  key_name               = aws_key_pair.key.key_name
  vpc_security_group_ids = [aws_security_group.basic.id]

  tags = {
    Name = "node-server"
  }
}