resource "local_file" "ansible_inventory" {
  content = templatefile("ansible_template.tpl",
    {
      front = aws_instance.front_server
      node  = aws_instance.node_server
    }
  )
  filename = "ansible/inventory/hosts.yml"
}