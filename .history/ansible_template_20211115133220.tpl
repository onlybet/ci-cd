all:
  children:
    front:
      hosts:
        ${front.tags["Name"]}:
          ansible_host: ${front.public_ip}
    runner:
      hosts:
%{ for server in node ~}
        ${server.tags["Name"]}:
          ansible_host: ${server.public_ip}
%{ endfor ~}