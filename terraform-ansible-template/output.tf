resource "local_file" "ansible_inventory" {
  content = templatefile("ansible_template.tpl",
    {
      front = aws_instance.front_server
      back = aws_instance.back_server
    }
  )
  filename = "ansible/inventory/hosts.yml"
}

output "front_id" {
  description = "ID of the FRONT EC2 instance"
  value       = aws_instance.front_server.id
}

output "front_public_ip" {
  description = "Public IP address of the FRONT EC2 instance"
  value       = aws_instance.front_server.public_ip
}

output "back_id" {
  description = "ID of the BACK EC2 instance"
  value       = aws_instance.back_server.id
}

output "back_public_ip" {
  description = "Public IP address of the BACK EC2 instance"
  value       = aws_instance.back_server.public_ip
}
